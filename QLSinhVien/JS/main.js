var danhSachSinhVien = [];

var validatorSV = new ValidatorSV();

const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

const timKimViTri = function (id, array) {
  return array.findIndex(function (sv) {
    return sv.maSv === id;
  });
};

const luuLocalStroge = function () {
  var dssvJson = JSON.stringify(danhSachSinhVien);
  localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
};

//lấy dữ liệu từ localstorage khi user tải lại trang
var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
// Gán cho array gôc và render lại giao diện
if (dssvJson) {
  danhSachSinhVien = JSON.parse(dssvJson);
  danhSachSinhVien = danhSachSinhVien.map(function (item) {
    return new SinhVien(
      item.maSv,
      item.tenSv,
      item.emailSv,
      item.diemToan,
      item.diemLy,
      item.diemHoa
    );
  });
  xuatDanhSachSinhVien(danhSachSinhVien);
}

function themSinhVien() {
  var newSinhVien = layThongTinTuForm();

  var isValid = true;

  var index = danhSachSinhVien.findIndex(function (item) {
    return item.maSv == newSinhVien.maSv;
  });

  isValidMaSv =
    validatorSV.kiemTraRong(
      "txtMaSV",
      "spanMaSV",
      "Mã Sinh Viên khum được để rỗng."
    ) && validatorSV.kiemTraIdHopLe(newSinhVien, danhSachSinhVien);

  isValidEmailSv = validatorSV.kiemTraEmail("txtEmail", "spanEmailSV");

  isValidTenSV = validatorSV.kiemTraTen("txtTenSV", "spanTenSV");

  isValidDiemToan =
    validatorSV.kiemTraRong(
      "txtDiemToan",
      "spanToan",
      "Điểm Toán khum được để rỗng."
    ) && validatorSV.kiemTraDiem("txtDiemToan", "spanToan");

  isValidDiemLy =
    validatorSV.kiemTraRong(
      "txtDiemLy",
      "spanLy",
      "Điểm Lý khum được để rỗng."
    ) && validatorSV.kiemTraDiem("txtDiemLy", "spanLy");

  isValidDiemHoa =
    validatorSV.kiemTraRong(
      "txtDiemHoa",
      "spanHoa",
      "Điểm Hóa khum được để rỗng."
    ) && validatorSV.kiemTraDiem("txtDiemHoa", "spanHoa");

  isValid =
    isValidMaSv &&
    isValidEmailSv &&
    isValidTenSV &&
    isValidDiemToan &&
    isValidDiemLy &&
    isValidDiemHoa;

  if (isValid) {
    danhSachSinhVien.push(newSinhVien);
    xuatDanhSachSinhVien(danhSachSinhVien);

    luuLocalStroge();
    //console.log(dssvJson);
  }
}

function xoaSinhVien(id) {
  var viTri = timKimViTri(id, danhSachSinhVien);

  danhSachSinhVien.splice(viTri, 1);

  xuatDanhSachSinhVien(danhSachSinhVien);
  luuLocalStroge();
}

function suaSinhVien(id) {
  var viTri = timKimViTri(id, danhSachSinhVien);
  var sinhVien = danhSachSinhVien[viTri];
  xuatThongTinLenForm(sinhVien);
}

function capNhatSinhVien() {
  var sinhVienEdit = layThongTinTuForm();

  let viTri = timKimViTri(sinhVienEdit.maSv, danhSachSinhVien);

  danhSachSinhVien[viTri] = sinhVienEdit;
  xuatDanhSachSinhVien(danhSachSinhVien);

  luuLocalStroge();
}

function timSinhvien() {
  var danhSachSinhVienCanTim = [];
  var sinhVienCanTim = document.getElementById("txtSearch").value.trim();

  danhSachSinhVien.forEach(function (item) {
    if (
      item.maSv == sinhVienCanTim ||
      item.tenSv == sinhVienCanTim ||
      item.emailSv == sinhVienCanTim
    ) {
      danhSachSinhVienCanTim.push(item);
    }

    xuatDanhSachSinhVien(danhSachSinhVienCanTim);
  });
}

function hienThiLaiDssvKhiXoaSearch() {
  var txtSearch = document.getElementById("txtSearch").value;
  if (txtSearch == "") {
    xuatDanhSachSinhVien(danhSachSinhVien);
  }
}

function resetInputDssv() {
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
}
