function SinhVien(_ma, _ten, _email, _toan, _ly, _hoa) {
  this.maSv = _ma;
  this.tenSv = _ten;
  this.emailSv = _email;
  this.diemToan = _toan;
  this.diemLy = _ly;
  this.diemHoa = _hoa;

  this.tinhDiemTrungBinh = function () {
    return (this.diemToan + this.diemLy + this.diemHoa) / 3;
  };
}
