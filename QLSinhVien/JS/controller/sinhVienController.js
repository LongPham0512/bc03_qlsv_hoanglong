function layThongTinTuForm() {
  var maSv = document.getElementById("txtMaSV").value;
  var tenSv = document.getElementById("txtTenSV").value;
  var emailSv = document.getElementById("txtEmail").value;
  var toan = document.getElementById("txtDiemToan").value * 1;
  var ly = document.getElementById("txtDiemLy").value * 1;
  var hoa = document.getElementById("txtDiemHoa").value * 1;

  var sinhVien = new SinhVien(maSv, tenSv, emailSv, toan, ly, hoa);
  //console.log({ sinhVien });
  return sinhVien;
}

function xuatDanhSachSinhVien(dssv) {
  var contentHTML = "";

  for (var index = 0; index < dssv.length; index++) {
    var sinhVien = dssv[index];

    var contentTrTag = `<tr>
        <td>${sinhVien.maSv}</td>
        <td>${sinhVien.tenSv}</td>
        <td>${sinhVien.emailSv}</td>
        <td>${sinhVien.tinhDiemTrungBinh()}</td>
        <td>
        <button class="btn btn-success" onclick="suaSinhVien('${
          sinhVien.maSv
        }')">Sửa</button>
        <button class="btn btn-danger" onclick="xoaSinhVien('${
          sinhVien.maSv
        }')">Xóa</button>
        </td>
      </tr>`;

    contentHTML += contentTrTag;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function xuatThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = sv.maSv;
  document.getElementById("txtTenSV").value = sv.tenSv;
  document.getElementById("txtEmail").value = sv.emailSv;
  document.getElementById("txtDiemToan").value = sv.diemToan;
  document.getElementById("txtDiemLy").value = sv.diemLy;
  document.getElementById("txtDiemHoa").value = sv.diemHoa;
}
